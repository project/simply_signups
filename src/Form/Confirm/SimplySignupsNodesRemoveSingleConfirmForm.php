<?php

namespace Drupal\simply_signups\Form\Confirm;

use Drupal\node\Entity\Node;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Defines a confirmation form to confirm deletion of something by id.
 */
class SimplySignupsNodesRemoveSingleConfirmForm extends ConfirmFormBase {

  /**
   * ID of the item to delete.
   *
   * @var int
   */
  protected $id;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, string $id = NULL) {
    $nid = \Drupal::routeMatch()->getParameter('node');
    $node = Node::load($nid);
    $isValidNode = (isset($node)) ? TRUE : FALSE;
    if (!$isValidNode) {
      throw new NotFoundHttpException();
    }
    $id = $node->id();
    $sid = \Drupal::routeMatch()->getParameter('sid');
    $this->id = $sid;

    $form['#attached']['library'][] = 'simply_signups/styles';
    $form['#attributes'] = [
      'class' => [
        'simply-signups-nodes-remove-confirm-form',
        'simply-signups-form',
      ],
    ];
    $form['title'] = [
      '#type' => 'hidden',
      '#value' => $this->id,
    ];
    $form['sid'] = [
      '#type' => 'hidden',
      '#value' => $sid,
    ];
    $form['nid'] = [
      '#type' => 'hidden',
      '#value' => $nid,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // @todo Do the deletion.
    $values = $form_state->getValues();
    $db = \Drupal::database();
    $db->delete('simply_signups_data')
      ->condition('id', $values['sid'], '=')
      ->condition('nid', $values['nid'], '=')
      ->execute();
    $form_state->setRedirect('simply_signups.nodes', ['node' => $values['nid']]);
    $this->messenger()->addMessage($this->t('Successfully removed Signup.'));
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() : string {
    return "simply_signups_nodes_remove_single_confirm_form";
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Do you want to remove signup: %id?', ['%id' => $this->id]);
  }

}
