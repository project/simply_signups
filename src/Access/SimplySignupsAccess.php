<?php

namespace Drupal\simply_signups\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\node\Entity\Node;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Access\AccessResultInterface;

/**
 * Check the access to a node task based on the node type.
 */
class SimplySignupsAccess {

  /**
   * Implements a custom access check.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   Access status for this node and user.
   */
  public function accessSettings(RouteMatchInterface $route_match): AccessResultInterface {
    $node = $route_match->getParameter('node');
    return $this->accessCheck($node, 'edit_simply_signups');
  }

  /**
   * Implements accessSignup permission check.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   Access status for this node and user.
   */
  public function accessSignup(RouteMatchInterface $route_match): AccessResultInterface {
    $node = $route_match->getParameter('node');
    return $this->accessCheck($node, 'view_simply_signups');
  }

  /**
   * Helper function to determine if the user has access.
   *
   * @param int $node
   *   Node to check access against.
   * @param string $permission
   *   Permission to use to check access.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   Access status for this node and user.
   */
  protected function accessCheck($node, $permission): AccessResultInterface {
    $account = \Drupal::currentUser();
    $nodeObject = Node::load($node);
    $node = $nodeObject;
    $config = \Drupal::config('simply_signups.config');
    $bundles = $config->get('bundles');
    $bundle = $node->bundle();
    $bundleAccess = (in_array($bundle, $bundles, TRUE)) ? 1 : 0;
    $permission = ($account->hasPermission($permission) == 1) ? 1 : 0;
    $y = 0;
    if (($permission == 1) and ($bundleAccess == 1)) {
      $y = 1;
    }
    return ($y == 1) ? AccessResult::allowed() : AccessResult::forbidden();
  }

}
