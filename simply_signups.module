<?php

/**
 * @file
 * Provides a signups node form.
 */

use Drupal\node\NodeInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\simply_signups\Utility\SimplySignupsUtility;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;

/**
 * Implements hook_entity_extra_field_info().
 */
function simply_signups_entity_extra_field_info() {
  $extra = [];
  $config = \Drupal::config('simply_signups.config');
  $bundles = $config->get('bundles');
  foreach ($bundles as $bundle) {
    $extra['node'][$bundle]['display']['simply_signups_form'] = [
      'label' => t('Sign up form'),
      'description' => t('This field variable is a placeholder for the signup form.'),
      'weight' => 0,
      'visible' => TRUE,
    ];
  }
  return $extra;
}

/**
 * Implements hook_node_view().
 */
function simply_signups_node_view(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, $view_mode) {
  /* Get NID of currently displaying node */
  $node = \Drupal::routeMatch()->getParameter('node');
  if ($node instanceof NodeInterface) {
    $nid = $node->id();
  }
  // Check if current node is displaying the FULL page view mode */.
  if ($view_mode == 'full') {
    $bundle = $entity->bundle();
    $config = \Drupal::config('simply_signups.config');
    $bundles = $config->get('bundles');
    /* check if content type is enabled in simply signups configuration */
    if (in_array($bundle, $bundles, TRUE)) {
      $build['#cache']['max-age'] = 0;
      \Drupal::service('page_cache_kill_switch')->trigger();
      $db = \Drupal::database();
      $query = $db->select('simply_signups_settings', 'p');
      $query->fields('p');
      $query->condition('nid', $nid, '=');
      $count = $query->countQuery()->execute()->fetchField();
      if ($count > 0) {
        $results = $query->execute()->fetchAll();
        $nodeSettings = [];
        foreach ($results as $row) {
          $nodeSettings['nid'] = $row->nid;
          $nodeSettings['startDate'] = $row->start_date;
          $nodeSettings['endDate'] = $row->end_date;
          $nodeSettings['maxSignups'] = $row->max_signups;
          $nodeSettings['status'] = $row->status;
        }
        /* signup is enabled for this node */
        if ($nodeSettings['status'] == 1) {
          $now = \Drupal::time()->getCurrentTime();
          /* signup is displayed if the current datetime is before start datetime */
          if (($now > $nodeSettings['startDate']) and ($now < $nodeSettings['endDate'])) {
            $numberOfAttending = SimplySignupsUtility::getNumberOfAttending($nid);
            $maxAttending = SimplySignupsUtility::getMaxAttending($nid);
            /* Max number attending is unlimited or Number of attending
             * is less than max number attending.
             */
            if ($numberOfAttending < $maxAttending or $maxAttending == 0) {
              $formObject = 'Drupal\simply_signups\Form\SimplySignupsForm';
              $form = \Drupal::formBuilder()->getForm($formObject, $nid);
              $build['simply_signups_form'] = [
                '#markup' => render($form),
              ];
            }
          }
        }
      }
    }
  }
}

/**
 * Implements hook_help().
 */
function simply_signups_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the simply signups module.
    case 'help.page.simply_signups':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Event signup form') . '</p>';
      return $output;

    default:
  }

}
